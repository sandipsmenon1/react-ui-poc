import React from 'react'
import Typography from '@material-ui/core/Typography'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import TableFooter from '@material-ui/core/TableFooter'
import TablePagination from '@material-ui/core/TablePagination'
import Paper from '@material-ui/core/Paper'
import { makeStyles } from '@material-ui/core/styles'
import Icons, { PanelIcons } from '../icons/icons'
import clsx from 'clsx'

const useStyles = makeStyles({
  root: {
    width: '100%',
    marginTop: '40px'
  },
  table: {
  },
  iconGrocery: {
    color: '#ff6600',
    fontSize: '2.5rem'
  },
  iconPayment: {
    color: '#CC66FF',
    fontSize: '2.5rem'
  },
  iconTransport: {
    color: '#0052cc',
    fontSize: '2.5rem'
  },
  iconEntertainment: {
    color: '#990099',
    fontSize: '2.5rem'
  },
  iconFood: {
    color: '#339933',
    fontSize: '2.5rem'
  },
  panelHeader: {
    float: 'left',
    padding: '15px',
    marginTop: '-50px',
    marginLeft: '15px',
    borderRadius: '3px'
  },
  tableHeader: {
    background: 'linear-gradient(60deg, #ec407a, #d81b60)',
    boxShadow: '0 4px 20px 0 rgba(0, 0, 0,.14), 0 7px 10px -5px rgba(233, 30, 99,.4)'
  },
  subtitle1: {
    paddingTop: '10px',
    paddingRight: '15px',
    display: 'table-caption'
  }
})

export default function TableComponent ({ title, message, rows }) {
  const classes = useStyles()
  const headerClass = clsx(classes.panelHeader, classes.tableHeader)

  return (<Paper className={classes.root}>
    <Table className={classes.table} aria-label="simple table">
      <Typography component="div" variant="h3" color="primary" className={headerClass}>
        <PanelIcons category={'table'} />
      </Typography>
      <Typography color="textSecondary" component="div" variant="subtitle1" className={classes.subtitle1} align="right">
        {'Transaction Report'}
      </Typography>
      <TableBody>
        {rows.map(row => (
          <TableRow key={row.date}>
            <TableCell component="th" scope="row">
              <Icons category={row.category} classes={ classes} /> {row.category}
            </TableCell>
            <TableCell align="right">{row.date}</TableCell>
            <TableCell align="right">{row.description}</TableCell>
            <TableCell align="right">{row.amount}</TableCell>
          </TableRow>
        ))}
      </TableBody>
      <TableFooter>
        <TableRow>
          <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            colSpan={3}
            count={rows.length}
            rowsPerPage={5}
            page={0}
            SelectProps={{
              inputProps: { 'aria-label': 'rows per page' },
              native: true
            }}
          />
        </TableRow>
      </TableFooter>
    </Table>
  </Paper>)
}
