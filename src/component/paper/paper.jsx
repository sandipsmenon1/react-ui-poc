import React from 'react'
import Typography from '@material-ui/core/Typography'
import { PanelIcons } from '../icons/icons'
import { makeStyles } from '@material-ui/core/styles'
import clsx from 'clsx'

const useStyles = makeStyles({
  panelHeader: {
    float: 'left',
    padding: '15px',
    marginTop: '-20px',
    marginRight: '15px',
    borderRadius: '3px'
  },
  paperContainer: {
    textAlign: 'right'
  },
  moneyLeft: {
    background: 'linear-gradient(60deg, #ffa726, #fb8c00)',
    boxShadow: '0 4px 20px 0 rgba(0, 0, 0,.14), 0 7px 10px -5px rgba(255, 152, 0,.4)'
  },
  moneyIn: {
    background: 'linear-gradient(60deg, #66bb6a, #43a047)',
    boxShadow: '0 4px 20px 0 rgba(0, 0, 0,.14), 0 7px 10px -5px rgba(76, 175, 80,.4)'
  },
  moneyOut: {
    background: 'linear-gradient(60deg, #26c6da, #00acc1)',
    boxShadow: '0 4px 20px 0 rgba(0, 0, 0,.14), 0 7px 10px -5px rgba(0, 172, 193,.4)'
  },
  savings: {
    background: 'linear-gradient(60deg, #ef5350, #e53935)',
    boxShadow: '0 4px 20px 0 rgba(0, 0, 0,.14), 0 7px 10px -5px rgba(244, 67, 54,.4)'
  },
  moneyChart: {
    maxWidth: '350px',
    marginLeft: 'inherit'
  },
  paperPanel: {
    marginBottom: '1em'
  }
})

export default function PaperPanel ({ title, message, className, category }) {
  const classes = useStyles()
  const headerClass = clsx(classes.panelHeader, classes[category])
  return (
    <React.Fragment>
      <Typography component="div" variant="h3" color="primary" className={classes.paperContainer}>
        <Typography component="div" variant="h3" color="primary" className={headerClass}>
          <PanelIcons category={category} />
        </Typography>
        <Typography color="textSecondary" className={classes.paperPanel}>
          { title }
        </Typography>
        <Typography component="p" variant="h4" color='primary'>
          {message}
        </Typography>
      </Typography>
    </React.Fragment>
  )
}
