import React from 'react'
import LocalGroceryStoreIcon from '@material-ui/icons/LocalGroceryStore'
import PaymentIcon from '@material-ui/icons/Payment'
import CommuteIcon from '@material-ui/icons/Commute'
import CenterFocusWeakIcon from '@material-ui/icons/CenterFocusWeak'
import FastfoodIcon from '@material-ui/icons/Fastfood'
import { makeStyles } from '@material-ui/core/styles'
import AccountBalanceIcon from '@material-ui/icons/AccountBalance'
import LaunchIcon from '@material-ui/icons/Launch'
import BarChartIcon from '@material-ui/icons/BarChart'
import ViewListIcon from '@material-ui/icons/ViewList'
import DashboardIcon from '@material-ui/icons/Dashboard'
import NoteAddIcon from '@material-ui/icons/NoteAdd'

const iconSize = {
  height: '1em',
  width: '2em'
}

const useStyles = makeStyles({
  root: {
    width: '100%',
    overflowX: 'auto',
    marginTop: '40px'
  },
  iconGrocery: {
    color: '#ff6600',
    fontSize: '2.5rem',
    ...iconSize
  },
  iconPayment: {
    color: '#CC66FF',
    fontSize: '2.5rem',
    ...iconSize
  },
  iconTransport: {
    color: '#0052cc',
    fontSize: '2.5rem',
    ...iconSize
  },
  iconEntertainment: {
    color: '#990099',
    fontSize: '2.5rem',
    ...iconSize
  },
  iconFood: {
    color: '#339933',
    fontSize: '2.5rem',
    ...iconSize
  },
  defaultIcon: {
    color: '#fff',
    width: '2em',
    height: '1.5em'
  }
})

const Icons = ({ category }) => {
  const classes = useStyles()
  switch (category) {
    case 'Grocery':
      return <LocalGroceryStoreIcon className={classes.iconGrocery} />
    case 'Transport':
      return <CommuteIcon className={classes.iconTransport} />
    case 'Entertainment':
      return <CenterFocusWeakIcon className={classes.iconEntertainment} />
    case 'Food':
      return <FastfoodIcon className={classes.iconFood} />
    case 'Finance':
      return <PaymentIcon className={classes.iconPayment} />
    default:
      return ''
  }
}

export const PanelIcons = ({ category }) => {
  const classes = useStyles()
  switch (category) {
    case 'moneyOut':
      return <LaunchIcon className={classes.defaultIcon} />
    case 'moneyIn':
      return <BarChartIcon className={classes.defaultIcon} />
    case 'moneyLeft':
      return <AccountBalanceIcon className={classes.defaultIcon} />
    case 'table':
      return <ViewListIcon className={classes.defaultIcon} />
    case 'chart':
      return <DashboardIcon className={classes.defaultIcon} />
    case 'savings':
      return <NoteAddIcon className={classes.defaultIcon} />
    default:
      return undefined
  }
}

export default Icons
