import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams
} from "react-router-dom";

const Header = (props) => (
<div>
<div class="top-bar py-2 border-bottom">
			<div class="container">
				<div class="row middle-flex">
          <div class="col-8" style={{textAlign:'left'}}>
              <div class="row">

                  <ul>
                    <li class="current">Personal</li>
                    <li><a href="">Business</a></li>
                    <li><a href="">Private Banking</a></li>
                    <li><a href="">International Banking</a></li>
                  </ul>
              </div>
          </div>
          <div class="col-4" style={{textAlign:'right', padding:'0px 5px'}}>
              <div style={{float:'right'}}>
                    <Link to="/Login"><a href="" class="btn login-button-2 text-uppercase text-wh">
                      <span class="fa fa-sign-in mr-2"></span>Login</a></Link>
              </div>
              <div style={{float:'right', padding:'0px 5px'}}>
                    <a href="" class="btn login-button-2 text-uppercase text-wh">
                      <span class="fa fa-sign-in mr-2"></span>Contact</a>
              </div>
          </div>
				</div>
			</div>
</div>
<div class="main-top py-1">
		<div class="container">
			<div class="nav-content">
				<h1>
					<Link to=""><img src={require('./Neobank_final.png')} style={{width:'70%'}} /></Link>
				</h1>
				<div class="nav_web-dealingsls">
					<nav>
						<label for="drop" class="toggle">Menu</label>
						<input type="checkbox" id="drop" />
						<ul class="menu">
            <li>
            <label for="drop-1" class="toggle toogle-2">Products and services <span class="fa fa-angle-down"
                aria-hidden="true"></span>
            </label>
            <a href="#">Products and services</a>
            <input type="checkbox" id="drop-1" />
            <ul>
              <li><a class="drop-text" href="#">Current accounts</a></li>
              <li><a class="drop-text" href="#">Credit cards</a></li>
              <li><a class="drop-text" href="#">Saving accounts</a></li>
              <li><a class="drop-text" href="#">Investing</a></li>
              <li><a class="drop-text" href="#">Retirement</a></li>
              <li><a class="drop-text" href="#">Loans</a></li>
            </ul>
            </li>
							<li>
              <label for="drop-2" class="toggle toogle-2">Help and Support <span class="fa fa-angle-down"
                  aria-hidden="true"></span>
              </label>
              <a href="#">Help and Support</a>
              <input type="checkbox" id="drop-2" />
              <ul>
                <li><a class="drop-text" href="#">By your side</a></li>
                <li><a class="drop-text" href="#">Customer support</a></li>
                <li><a class="drop-text" href="#">Service status</a></li>
                <li><a class="drop-text" href="#">Ask the experts</a></li>
                <li><a class="drop-text" href="#">Product support</a></li>
              </ul>
              </li>
							<li>
								<label for="drop-3" class="toggle toogle-2">Banking with us <span class="fa fa-angle-down"
										aria-hidden="true"></span>
								</label>
								<a href="#pages">Banking with us <span class="fa fa-angle-down" aria-hidden="true"></span></a>
								<input type="checkbox" id="drop-3" />
								<ul>
									<li><a class="drop-text" href="#">Ways to bank</a></li>
									<li><a class="drop-text" href="#">Who we are</a></li>
									<li><a class="drop-text" href="#">In your community</a></li>
									<li><a class="drop-text" href="#">Mobile branches</a></li>
								</ul>
							</li>
							<li><a href="">Banking online</a></li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div>
</div>
)

export default Header;
