import React from 'react';
import { Link } from 'react-router-dom';

import { CarouselContainer, ImgWrapper, Container, Indicator } from './styles';
import leftarrow from '../left-arrow.svg';
import rightarrow from '../right-arrow.svg';

class Carousel extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      currentIndex: 0,
      images: this.props.banners,
      translateValue: 0,
      transitionDuration: 1,
    };
  }

  componentDidMount() {
      if (this.props.banners.length > 1){
        this.slideInterval = setInterval(this.nextImage, 4000);
    }
  }
  componentWillUnmount() {
    if (this.state.images.length > 1) clearInterval(this.slideInterval);
  }

  resetInterval = (reset = true) => {
    clearInterval(this.slideInterval);
    if (reset) this.slideInterval = setInterval(this.nextImage, 3000);
  };

  doSliding = (currentIndex, translateValue, transitionDuration) => {
    this.setState({
      currentIndex,
      translateValue,
      transitionDuration,
    });
    this.resetInterval();
  };

  goToCard = index => {
    this.doSliding(index, index * -100, 1);
  };

  nextImage = () => {
    let { currentIndex, translateValue, transitionDuration } = this.state;
    const imagesCount = this.state.images.length;

    if (currentIndex >= imagesCount - 1) {
      currentIndex = 0;
      translateValue = 0;
      transitionDuration = 0.4;
    } else {
      currentIndex += 1;
      translateValue -= 100;
      transitionDuration = 1;
    }
    this.doSliding(currentIndex, translateValue, transitionDuration);
  };

  prevImage = () => {
    let { currentIndex, translateValue, transitionDuration } = this.state;
    const imagesCount = this.state.images.length;

    if (currentIndex === 0) {
      currentIndex = imagesCount - 1;
      translateValue = (imagesCount - 1) * -100;
      transitionDuration = 0.4;
    } else {
      currentIndex -= 1;
      translateValue += 100;
      transitionDuration = 1;
    }
    this.doSliding(currentIndex, translateValue, transitionDuration);
  };

  render() {
    return (
      <CarouselContainer
        onMouseOver={() => this.resetInterval(false)}
        onMouseOut={this.resetInterval}
        style={this.props.style}
      >
        <ImgWrapper
          transitionDuration={this.state.transitionDuration}
          translateValue={this.state.translateValue}
        >
          {this.state.images.length &&
            this.state.images.map(
              i =>
                <Link
                className="slideImages"
                    to={i.redirect_url.substr(i.redirect_url.indexOf('/', 8))}
                    key={i.id}
                  >
                    <div className='text'><h1>{i.text}</h1></div>
                    <div className='text2'>{i.text2}</div>
                    <img
                      className="image"
                      src={i.image_url}
                      alt={i.name}
                    />
                  </Link>

            )}
        </ImgWrapper>

        {this.state.images.length > 1 && (
          <React.Fragment>
            <button onClick={this.prevImage} className="navArrow left">
              <img src={leftarrow} style={{width:'30%'}} alt='Left arrow' />
            </button>
            <button onClick={this.nextImage} className="navArrow right">
              <img src={rightarrow} style={{width:'30%'}} alt='Right arrow' />
            </button>

            <Container>
              {Array(this.state.images.length)
                .fill()
                .map((indicator, i) => (
                  <Indicator
                    onClick={() => this.goToCard(i)}
                    key={i}
                    isCurrent={i === this.state.currentIndex}
                  />
                ))}
            </Container>
          </React.Fragment>
        )}
      </CarouselContainer>
    );
  }
}

Carousel.defaultProps = {
  style: {},
};
export default Carousel;
