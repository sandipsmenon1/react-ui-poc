import styled from 'styled-components';

export const CarouselContainer = styled.div`
  position: relative;
  width: 100%;
  height: auto;
  .navArrow {
    position: absolute;
    border: none;
    top: 0;
    left: 0;
    bottom: 0;
    width: 60px;
    opacity: 0.5;
    border-radius: 0px;
    filter: alpha(opacity=50);
    background-color: rgba(0, 0, 0, 0);
    outline: none;
    &:hover {
      cursor: pointer;
      opacity: 0.9;
    }
    @media (min-width: 768px) {
      width: 12%;
    }
  }
  .left {
    &:hover {
      background-image: linear-gradient(
        to left,
        rgba(0, 0, 0, 0.0001) 0,
        rgba(0, 0, 0, 0.15) 100%
      );
    }
  }
  .right {
    left: auto;
    right: 0;
    &:hover {
      background-image: linear-gradient(
        to right,
        rgba(0, 0, 0, 0.0001) 0,
        rgba(0, 0, 0, 0.15) 100%
      );
    }
  }
`;

export const ImgWrapper = styled.div`
  display: flex;
  flex-direction: row;
  height: inherit;
  border-radius: none;
  overflow: hidden;
  max-height:400px;
  .text {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    color:white;
    background:black;opacity:0.5;
    padding:0px 20px;
    text-align:left;
  }
  .text2{
    position: absolute;
    width: 40%;
    height: 100%;
    color:white;
    padding:60px 10px 0px 20px;
    font-size: 15px;
    text-align:left;
  }
  a {
    display: inline-block;
    width: 100%;
    height: 100%;
    flex: 0 0 100%;
    .image {
      width: 100%;
      height: 100%;
    }
  }
  .slideImages {
    border-style: none;
    border-radius: 10px;
    transition: ${props =>
    props.sliding ? 'none' : `transform ${props.transitionDuration}s ease`};
    transform: translateX(${props => `${props.translateValue}%` || 0});
  }
`;

export const Container = styled.div`
  width: 100px;
  position: absolute;
  left: calc(50% - 50px);
  bottom: 0;
  text-align: center;
  margin-bottom: 10px;
  z-index: 15;
`;

export const Indicator = styled.span`
  background-color: ${props => (props.isCurrent ? '#fff' : 'transparent')};
  width: 12px;
  height: 12px;
  border-radius: 10px;
  border: 1px solid #fff;
  margin: 5px;
  display: inline-block;
  transition: background 0.5s ease;
  cursor: pointer;
`;
