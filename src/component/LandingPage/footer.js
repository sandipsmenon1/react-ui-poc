import React from 'react';

const Footer = () => (
  <div class="footer">
  <div class="cpy-right text-center py-3">
		<p>© 2019 Neobank. All rights reserved</p>
	</div>
  </div>
)

export default Footer;
