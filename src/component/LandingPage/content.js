import React from 'react';
import ReactBootstrapCarousel from "react-bootstrap-carousel";
import "bootstrap/dist/css/bootstrap.css";
import './index.css';
import "react-bootstrap-carousel/dist/react-bootstrap-carousel.css";
const Content = () => {
  return (
<div class="content">
  
<section class="blog_w3ls py-5">
		<div class="container pb-xl-5 pb-lg-3">
			<div class="row">
				<div class="col-lg-4 col-md-6">
					<div class="card border-0 med-blog">
						<div class="card-header p-0">

						</div>
						<div class="card-body border border-top-0">
							<h5 class="blog-title card-title m-0"><a href="">Our products</a></h5>
							<p class="mt-3">Current Accounts</p>
              <p class="mt-3">Credit Cards</p>
              <p class="mt-3">Loans</p>
							<a href="" class="btn button-w3ls mt-4 mb-3">View More
								<span class="fa fa-caret-right ml-1" aria-hidden="true"></span>
							</a>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 mt-md-0 mt-5">
					<div class="card border-0 med-blog">
						<div class="card-header p-0">

						</div>
						<div class="card-body border border-top-0">
							<h5 class="blog-title card-title m-0"><a href="">Help and Support</a></h5>
							<p class="mt-3">Customer support</p>
              <p class="mt-3">Unrecognised transaction</p>
              <p class="mt-3">Rates and charges</p>
							<a href="" class="button-w3ls active mt-4 mb-3">View More
								<span class="fa fa-caret-right ml-1" aria-hidden="true"></span>
							</a>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 mt-lg-0 mt-5">
					<div class="card border-0 med-blog">
						<div class="card-header p-0">

						</div>
						<div class="card-body border border-top-0">
							<h5 class="blog-title card-title m-0"><a href="">Internet Banking</a></h5>
							<p class="mt-3">Online help</p>
              <p class="mt-3">Mobile banking</p>
              <p class="mt-3">Self service</p>
							<a href="" class="button-w3ls mt-4 mb-3">View More
								<span class="fa fa-caret-right ml-1" aria-hidden="true"></span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
    </div>
  )

}


export default Content;
