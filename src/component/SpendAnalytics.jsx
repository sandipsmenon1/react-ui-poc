import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Container from '@material-ui/core/Container'
import { withStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'
import clsx from 'clsx'
import Typography from '@material-ui/core/Typography'
import PaperPanel from './paper/paper'
import spendItems from '../data-objects/dashboard.json'
import TableComponent from './table/table'
import SimplePieChart from './charts/pie-chart'
import SimpleBarChart, { BarChartComponent } from './charts/bar-chart'
import { PanelIcons } from './icons/icons'
import AccountLabel from './account/account-label'
import analyticsData from '../data-objects/spendAnalytics.json'
import Header from './header/header'

const style = theme => ({

  container: {
    margin: '50px',
    paddingTop: '20px',
    paddingBottom: '20px',
    maxWidth: '95%'
  },
  paper: {
    padding: '10px',
    display: 'flex',
    flexDirection: 'column'
  },
  fixedHeight: {
    height: 120,
    width: 300
  },
  panelHeader: {
    float: 'left',
    padding: '15px',
    marginTop: '-20px',
    marginRight: '15px',
    borderRadius: '3px'
  },
  paperContainer: {
    textAlign: 'right'
  },
  chartContainer: {
    marginTop: '40px'
  },
  chartMarginIcon: {
    marginLeft: '15px'
  },
  tableHeader: {
    background: 'linear-gradient(60deg, #ec407a, #d81b60)',
    boxShadow: '0 4px 20px 0 rgba(0, 0, 0,.14), 0 7px 10px -5px rgba(233, 30, 99,.4)'
  },
  chartPaperContainer: {
    height: '485px'
  },
  analyticsContainer: {
    marginTop: '20px'
  },
  subtitle1: {
    paddingTop: '10px',
    paddingRight: '15px'
  }

})
class SpendAnalytics extends Component {
  render () {
    const { classes } = this.props
    const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight)
    const headerChartPanel = clsx(classes.panelHeader, classes.tableHeader, classes.chartMarginIcon)
    return (<div >
      <Header />
      <main className={classes.content}>
        <Container maxWidth="lg" className={classes.container}>
          <AccountLabel name={analyticsData.activeAccount.accountName} />
          <hr/>
          <Grid container spacing={6} className={classes.analyticsContainer}>
            <Grid item xs={12} md={4} lg={3}>
              <Paper className={fixedHeightPaper}>
                <PaperPanel title={'Money Left'} message={'£2830.25'} category={'moneyLeft'} />
              </Paper>
            </Grid>
            <Grid item xs={12} md={4} lg={3}>
              <Paper className={fixedHeightPaper}>
                <PaperPanel title={'Money In'} message={'£4360.50'} category={'moneyIn'}/>
              </Paper>
            </Grid>
            <Grid item xs={12} md={4} lg={3}>
              <Paper className={fixedHeightPaper}>
                <PaperPanel title={'Money Out'} message={'£1530.25'} category={'moneyOut'}/>
              </Paper>
            </Grid>
            <Grid item xs={12} md={4} lg={3}>
              <Paper className={fixedHeightPaper}>
                <PaperPanel title={'Savings'} message={'£400.00'} category={'savings'}/>
              </Paper>
            </Grid>
          </Grid>
          <Grid container spacing={10}>
            <Grid item xs={24} md={8} lg={6}>
              <TableComponent rows={spendItems} />
            </Grid>
            <Grid item xs={24} md={8} lg={6} className={classes.chartContainer}>
              <Paper className={classes.chartPaperContainer}>
                <Typography component="div" className={headerChartPanel}>
                  <PanelIcons category={'chart'} />
                </Typography>
                <Typography color="textSecondary" variant="subtitle1" className={classes.subtitle1} align="right">
                  {'Spend Analysis report'}
                </Typography>
                <BarChartComponent rows={spendItems} />
              </Paper>
            </Grid>
          </Grid>
        </Container>
      </main>
    </div>)
  }
}

SpendAnalytics.propTypes = {
  initialValues: PropTypes.string
}

SpendAnalytics.defaultProps = {
  initialValues: ''
}

export default withStyles(style)(SpendAnalytics)
