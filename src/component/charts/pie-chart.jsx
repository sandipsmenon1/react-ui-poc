import React from 'react'
import { PieChart, Pie, Sector, Cell } from 'recharts'

const dummydata = [{ name: 'Group A', value: 400 }, { name: 'Group B', value: 300 },
  { name: 'Group C', value: 300 }, { name: 'Group D', value: 200 }]

const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042']

const RADIAN = Math.PI / 180
const renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, percent, index }) => {
  const radius = innerRadius + (outerRadius - innerRadius) * 0.5
  const x = cx + radius * Math.cos(-midAngle * RADIAN)
  const y = cy + radius * Math.sin(-midAngle * RADIAN)

  return (
    <text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
      {`${(percent * 100).toFixed(0)}%`}
    </text>
  )
}

const SimplePieChart = ({ data }) => {
  return (<PieChart width={400} height={150}>
    <Pie
      data={dummydata}
      cx={100}
      cy={50}
      labelLine={false}
      label={renderCustomizedLabel}
      outerRadius={60}
      fill="#8884d8"
    >{
        dummydata.map((entry, index) => <Cell fill={COLORS[index % COLORS.length]}/>)
      }
    </Pie>
  </PieChart>)
}

export default SimplePieChart
