
import React from 'react'
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, Cell } from 'recharts'
const data = [
  { name: 'Money Left', amt: 2830.25 },
  { name: 'Money In', amt: 4360.50 },
  { name: 'Money Out', amt: 1530.25 }
]

const spendAnalysis = [{ category: 'finance', amount: 220.00 },
  { category: 'grocery', amount: 115.50 },
  { category: 'transport', amount: 90.00 },
  { category: 'entertainment', amount: 173.00 },
  { category: 'food', amount: 120.00 }]

const barColors = [{ fill: '#5F277D', border: '#481167' },
  { fill: '#C13234', border: '#B02E30' },
  { fill: '#A7BF50', border: '#95AA46' },
  { fill: '#2B9C0A', border: '#2B9C0A' },
  { fill: '#0A159C', border: '#0A159C' },
  { fill: '#4F02F7', border: '#4F02F7' },
  { fill: '#1102F7', border: '#1102F7' }]

const SimpleBarChart = () => {
  return (
    <BarChart width={650} height={180} data={data}
      margin={{ top: 0, right: 0, left: 0, bottom: 0 }} barSize={10}>
      <CartesianGrid strokeDasharray="3 3"/>
      <XAxis dataKey="name"/>
      <YAxis/>
      <Tooltip/>
      <Legend />
      <Bar dataKey="amt" fill="#82ca9d" />
    </BarChart>
  )
}
export default SimpleBarChart

export const BarChartComponent = () => {
  return (
    <BarChart width={650} height={420} data={spendAnalysis}
      margin={{ top: 20, right: 10, left: 10, bottom: 0 }} barSize={10}>
      <CartesianGrid strokeDasharray="3 3"/>
      <XAxis dataKey="category"/>
      <YAxis dataKey="amount"/>
      <Tooltip/>
      <Bar dataKey="amount">
        {
          spendAnalysis.map((entry, index) => (
            <Cell key={`cell-${index}`} fill={barColors[index].fill} stroke={barColors[index].border} />
          ))
        }
      </Bar>
    </BarChart>
  )
}
