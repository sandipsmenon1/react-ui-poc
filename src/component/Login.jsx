import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams
} from "react-router-dom";

import Header from './LandingPage/header';
import Footer from './LandingPage/footer';
import './LandingPage/index.css';


class Login extends Component {

constructor(props) {
    super(props);
    this.state = {};
}

render() {
    console.log(this.props.initialValues);
    return (<div>
      <Header/>
      <div style={{textAlign:'left'}}>
      <div class="login-contect py-5">
      <div class="container py-xl-5 py-3">
        <div class="login-body">
          <div class="login p-4" style={{marginLeft:'0'}}>
            <h5 class="text-center mb-4">Welcome to Internet Banking</h5>
            <form>
              <div class="form-group">
                <label>Username</label>
                <input type="text" class="form-control" name="name" placeholder="" required=""/>
              </div>
              <div class="form-group">
                <label class="mb-2">Password</label>
                <input type="password" class="form-control" name="password" placeholder="" required=""/>
              </div>
              <button type="" class="btn submit mb-4" onClick={() => this.props.history.push('/dashboard')}>Login</button>
              <p class="forgot-w3ls text-center mb-3">
                <a href="#" class="text-da">Forgot your password?</a>
              </p>
              <p class="account-w3ls text-center text-da">
                Don't have an account?
                <a href="">Create new account</a>
              </p>
            </form>
          </div>
        </div>
      </div>
    </div>
      </div>
    <Footer/>

    </div>);
}

}

Login.propTypes = {
    initialValues: PropTypes.string,
}

Login.defaultProps = {
    initialValues: '',
}

export default Login;
