import React from 'react';
import '../App.css';
import Header from './header/header';

import {
    Button,
    IconButton,
    Link,
    Select,
    FormControl,
    InputLabel,
    Popover,
    Paper
} from '@material-ui/core';

import {
    Table,
    TableHead,
    TableBody,
    TableRow,
    TableCell,
    TablePagination,
    TableSortLabel
} from '@material-ui/core';

import {
    Search,
    Archive,
    AccessAlarm,
    Help,
    ArrowLeft,
    ArrowRight,
    KeyboardArrowDown,
    DateRange,
    ExpandMore,
} from '@material-ui/icons';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import PropTypes from 'prop-types';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import { wrap } from 'module';

export default class AccountDetails extends React.Component {
    constructor() {
        super();
        this.state = {
            anchorEL: null,
            transactionView: null,
            statementOption: "",
        }
    }

    handleClick = event => {
        alert(event.currentTarget);
        this.setState({ anchorEL: event.currentTarget });
    };

    handleClose = () => {
        this.setState({ anchorEL: null });
    };

    changeStatementDisplay = (displayMonth) => {
        this.setState({ ...this.state, transactionView: displayMonth });
    }

    updateStatementOptions = (value) => {
        this.setState({ ...this.state, statementOption: value });
    }

    render() {
        const linkStyle = {
            padding: '5px 0px 5px 0px',
            margin: '5px 5px 5px 25px'
        };
        const _2pxMargin = {
            margin: '2px',
        };
        const selectMargin = {
            marginTop: '10px',
            marginBottom: '2px',
            backgroundColor: 'white',
            borderRadius: '5px',
        };

        return (
           <div>
               <Header />
                <div className="accDet-container">
                <div className="row-layout" style={{justifyContent:'flex-end'}}>
                    <a  style={{fontSize:'15px', margin: '15px'}} href="#dashboard">
                        <ArrowBackIosIcon/>
                        Back to summary
                    </a>
                </div>
                <div className="row-layout" style={{ backgroundColor: '#eeeeee', borderRadius: '10px', padding: '10px', margin: '10px'}}>
                    <div className="col-layout" style={{ flex: 3 }}>
                        <div className="row-layout">
                            <div className="col-layout">
                                <Label textColor='blue' fontSize={20}>Current Account</Label>
                            </div>
                            <div className="col-layout">
                                <Label fontSize={16} boldText={false}>110006 12345678</Label>
                            </div>
                            <div className="col-layout">
                                <Link style={linkStyle}>View IBAN and BIC</Link>
                            </div>
                        </div>
                        <div className="row-layout">
                            <div className="col-layout">
                                <Label textColor='black' fontSize={26}>£10,000.00</Label>
                            </div>
                            <div className="col-layout">
                                <Label fontSize={16} boldText={false}>Balance</Label>
                            </div>
                        </div>
                        <div className="row-layout">
                            <div className="col-layout">
                                <Label textColor='black' fontSize={16}>£9,844.00</Label>
                            </div>
                            <div className="col-layout">
                                <div className="row-layout">
                                    <div className="col-layout">
                                        <Label fontSize={16} boldText={false}>Money Available</Label>
                                    </div>
                                    <div className="col-layout" style={{ justifyContent: 'center' }}>
                                        <HelpPopover headerText="Money Available" lableText="This is the amount you can spend now. It includes your agreed planned overdraft and has been reduced by any pending debit card transactions." />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-layout" style={{ flex: 1 }}>
                        <Button style={_2pxMargin} variant="contained" color="primary">
                            Make a Payment
              </Button>
                        <Button style={_2pxMargin} variant="contained" color="primary">
                            Upgrade Account
              </Button>
                        <Button style={_2pxMargin} variant="contained" color="primary">
                            More Actions
              </Button>
                    </div>
                </div>
                <div className="row-layout">
                    <div className="col-layout">
                        <br />
                        <Label textColor='black' boldText={false} fontSize={26}>Statement</Label>
                    </div>
                </div>
                <div className="col-layout" style={{ backgroundColor: '#eeeeee', borderRadius: '10px', padding: '10px', margin: '10px' }}>
                    <div className='row-layout' style={{ justifyContent: 'flex-end', flexWrap:"wrap" }}>
                        <MonthSlider clickAction={this.changeStatementDisplay} />
                        <Button style={_2pxMargin}
                            variant="contained"
                            color="primary"
                            onClick={() => this.changeStatementDisplay(null)}
                        > All Transactions </Button>
                        <Button style={_2pxMargin}
                            variant="contained"
                            color="primary"
                            endIcon={<Search />}
                        > Search </Button>
                    </div>
                    <div className='row-layout' style={{ justifyContent: 'space-between', flexWrap:'wrap' }}>
                        <div className="row-layout" style={{ alignItems: 'center', marginLeft: '10px' }}>
                            <DateRange />
                            <Label textColor='black' zeroMargin={true} boldText={false} fontSize={20}>{this.state.transactionView ? this.state.transactionView : 'All Transactions'}</Label>
                        </div>

                        <div className="col-layout">
                            <div className='row-layout'>
                                <div className='col-layout' style={{ justifyContent: 'center', margin: '5px' }}>
                                    <Archive />
                                </div>

                                <FormControl variant="outlined" style={selectMargin}>
                                    <InputLabel htmlFor="outlined-age-native-simple">
                                        Statement Options
                                        </InputLabel>
                                    <Select
                                        native
                                        value={this.state.statementOption}
                                        onChange={(event) => this.updateStatementOptions(event.target.value)}
                                        style={{ width: '230px' }}
                                        labelWidth={135}
                                        inputProps={{
                                            name: 'age',
                                            id: 'outlined-age-native-simple',
                                        }}
                                    >
                                        <option value="" />
                                        <option value={'Monthly PDFs'}>Montly PDFs</option>
                                        <option value={'Print Current'}>Print Current View</option>
                                        <option value={'Export'}>Export Transactions(CSV)</option>
                                        <option value={'Paper Statement'}>Order a paper statement</option>
                                    </Select>
                                </FormControl>
                            </div>
                        </div>
                    </div>
                    <div className='row-layout'>
                        <EnhancedTable />
                    </div>
                    <div className='row-layout'>
                        <PendingTransactionsPanel />
                    </div>
                </div>
            </div>
           </div>
        );
    }
}

function Label(props) {
    const labelStyle = {
        color: '#525151',
        fontWeight: 'bold',
        display: 'block',
        padding: '5px 0px 5px 0px'
    }
    const { children, warning = false, zeroMargin = false, fontSize = '16px', boldText = true, highlight = false, textColor = null } = props;

    return (
        <span style={{ ...labelStyle, margin: zeroMargin ? '0px 0px 0px 10px' : '5px 5px 5px 25px', fontSize: fontSize, color: warning ? '#FF0000' : highlight ? '#0000FF' : textColor ? textColor : '#525151', fontWeight: boldText ? 'bold' : 'normal' }}>{children}</span>
    );
}


function createData(date, desc, type, inp, out, bal) {
    return { date, desc, type, inp, out, bal };
}

const rows = [
    createData('01/01/2000', 'Tesco Purchse Liverpool', 'Debit', '', 12, 1200.00),
    createData('05/01/2000', 'London Underground', 'Debit', '', 10, 1200.00),
    createData('09/01/2000', 'Iceland', 'Debit', '', 13, 1200.00),
    createData('06/01/2000', 'M&S', 'Debit', '', 55, 1200.00),
    createData('02/01/2000', 'Uniqlo', 'Debit', '', 46, 1200.00),
    createData('11/01/2000', 'JD', 'Debit', '', 68, 1200.00),
    createData('29/01/2000', 'Merlins Attractions', 'Debit', '', 55, 1200.00),
    createData('18/01/2000', 'Cash from ATM Upton Park', 'Debit', '', 20, 1200.00),
    createData('16/01/2000', 'TCS Salary Credit', 'Credit', 3000, '', 4200.00),
    createData('12/01/2000', 'London Underground', 'Debit', '', 3, 1200.00),
    createData('22/01/2000', 'Tesco Purchase Stratford', 'Debit', '', 12, 1200.00),
    createData('20/01/2000', 'Amazon Purchase', 'Debit', '', 123, 1200.00),
    createData('27/01/2000', 'Europecar', 'Debit', '', 44, 1200.00),
];

function desc(a, b, orderBy) {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
}

function stableSort(array, cmp) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
        const order = cmp(a[0], b[0]);
        if (order !== 0) return order;
        return a[1] - b[1];
    });
    return stabilizedThis.map(el => el[0]);
}

function getSorting(order, orderBy) {
    return order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
}

const headCells = [
    { id: 'date', numeric: false, disablePadding: false, label: 'DATE' },
    { id: 'desc', numeric: false, disablePadding: false, label: 'DESCRIPTION' },
    { id: 'type', numeric: false, disablePadding: false, label: 'TYPE' },
    { id: 'inp', numeric: true, disablePadding: false, label: 'IN(£)' },
    { id: 'out', numeric: true, disablePadding: false, label: 'OUT(£)' },
    { id: 'bal', numeric: true, disablePadding: false, label: 'BALANCE' },
];

function EnhancedTable() {
    const classes = useStyles();
    const [order, setOrder] = React.useState('asc');
    const [orderBy, setOrderBy] = React.useState('date');
    const [selected, setSelected] = React.useState([]);
    const [page, setPage] = React.useState(0);
    const [dense] = React.useState(true);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);

    const CustomTablePagination = withStyles({
        toolbar: {
            flexWrap:'wrap',
        },
    })(TablePagination);

    const handleRequestSort = (event, property) => {
        const isDesc = orderBy === property && order === 'desc';
        setOrder(isDesc ? 'asc' : 'desc');
        setOrderBy(property);
    };

    const handleClick = (event, name) => {
        const selectedIndex = selected.indexOf(name);
        let newSelected = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, name);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1),
            );
        }

        setSelected(newSelected);
    };

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = event => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };

    const isSelected = name => selected.indexOf(name) !== -1;

    const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);

    return (
        <div className={classes.root}>
            <Paper className={classes.paper}>
                <div className={classes.tableWrapper}>
                    <Table
                        className={classes.table}
                        aria-labelledby="tableTitle"
                        size={dense ? 'small' : 'medium'}
                        aria-label="enhanced table"
                    >
                        <EnhancedTableHead
                            classes={classes}
                            order={order}
                            orderBy={orderBy}
                            onRequestSort={handleRequestSort}
                            rowCount={rows.length}
                        />
                        <TableBody>
                            {stableSort(rows, getSorting(order, orderBy))
                                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                .map((row, index) => {
                                    const isItemSelected = isSelected(row.name);
                                    const labelId = `enhanced-table-checkbox-${index}`;

                                    return (
                                        <TableRow
                                            hover
                                            onClick={event => handleClick(event, row.name)}
                                            role="checkbox"
                                            aria-checked={isItemSelected}
                                            tabIndex={-1}
                                            key={index}
                                            selected={isItemSelected}
                                        >
                                            <TableCell component="th" id={labelId} scope="row" padding="default">
                                                {row.date}
                                            </TableCell>
                                            <TableCell align="left">{row.desc}</TableCell>
                                            <TableCell align="left">{row.type}</TableCell>
                                            <TableCell align="right">{row.inp}</TableCell>
                                            <TableCell align="right">{row.out}</TableCell>
                                            <TableCell align="right">{row.bal}</TableCell>
                                        </TableRow>
                                    );
                                })}
                            {emptyRows > 0 && (
                                <TableRow style={{ height: (dense ? 33 : 53) * emptyRows }}>
                                    <TableCell colSpan={6} />
                                </TableRow>
                            )}
                        </TableBody>
                    </Table>
                </div>
                <CustomTablePagination
                    rowsPerPageOptions={[5, 10, 25]}
                    component="div"
                    count={rows.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    backIconButtonProps={{
                        'aria-label': 'previous page',
                    }}
                    nextIconButtonProps={{
                        'aria-label': 'next page',
                    }}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                />
            </Paper>
        </div>
    );
}

function EnhancedTableHead(props) {
    const { classes, order, orderBy, onRequestSort } = props;
    const createSortHandler = property => event => {
        onRequestSort(event, property);
    };
    return (
        <TableHead>
            <TableRow style={{ backgroundColor: '#3542b5' }}>
                {headCells.map(headCell => (
                    <TableCell style={{ borderTopLeftRadius: headCell.id === 'date' ? '5px' : '0px', borderTopRightRadius: headCell.id === 'bal' ? '5px' : '0px' }}
                        key={headCell.id}
                        align={headCell.numeric ? 'right' : 'left'}
                        padding={headCell.disablePadding ? 'none' : 'default'}
                        sortDirection={orderBy === headCell.id ? order : false}
                    >
                        <TableSortLabel
                            active={orderBy === headCell.id}
                            direction={order}
                            onClick={createSortHandler(headCell.id)}
                            style={{ color: 'white' }}
                            IconComponent={KeyboardArrowDown}
                        >
                            {headCell.label}
                            {orderBy === headCell.id ? (
                                <span className={classes.visuallyHidden}>
                                    {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                                </span>
                            ) : null}
                        </TableSortLabel>
                    </TableCell>
                ))}
            </TableRow>
        </TableHead>
    );
}

EnhancedTableHead.propTypes = {
    classes: PropTypes.object.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    order: PropTypes.oneOf(['asc', 'desc']).isRequired,
    orderBy: PropTypes.string.isRequired,
    rowCount: PropTypes.number.isRequired,
};

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing(3),
    },
    paper: {
        width: '100%',
        marginBottom: theme.spacing(2),
    },
    table: {
        minWidth: 750,
    },
    tableWrapper: {
        overflowX: 'auto',
    },
    visuallyHidden: {
        border: 0,
        clip: 'rect(0 0 0 0)',
        height: 1,
        margin: -1,
        overflow: 'hidden',
        padding: 0,
        position: 'absolute',
        top: 20,
        width: 1,
    },
}));

function HelpPopover(props) {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const { headerText, lableText } = props;
    const handleClick = event => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const open = Boolean(anchorEl);
    const id = open ? 'simple-popover' : undefined;

    return (
        <div>
            <Help fontSize="small" onClick={handleClick} />
            <Popover
                id={id}
                open={open}
                anchorEl={anchorEl}
                onClose={handleClose}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }}
            >
                <div style={{ width: '250px', padding: '5px' }}>
                    <Label fontSize={13} zeroMargin={true}>{headerText}</Label>
                    <Label fontSize={13} boldText={false} zeroMargin={true}>{lableText}</Label>
                </div>
            </Popover>
        </div>
    );
}

function MonthSlider(props) {
    const { viewItemCount = 3, clickAction } = props;
    const [start, setStart] = React.useState(12 - viewItemCount);
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    const currDate = new Date();
    const currMonth = currDate.getMonth();

    const [clicked, setClicked] = React.useState(monthNames[currMonth]);

    var monthsArr = new Array(12);
    for (var i = 0; i < 12; i++) {
        var pushedMonth = currMonth - i;
        var isPrevYear = false;
        if (pushedMonth < 0) {
            isPrevYear = true;
            pushedMonth = 12 - Math.abs(pushedMonth);
        }
        monthsArr.push(monthNames[pushedMonth] + (isPrevYear ? " " + ((currDate.getFullYear() - 1) + "").substring(2, 4) : ""));
    }
    monthsArr = monthsArr.reverse();

    const incrementStart = () => {
        setStart(start + viewItemCount);
    }

    const decrementStart = () => {
        setStart(start - viewItemCount);
    }

    const buttonClickAction = (clickedVal) => {
        setClicked(clickedVal);
        clickAction(clickedVal);
    }

    return (
        <div className="row-layout" style={{ backgroundColor: 'white', borderRadius: 5, margin: 2, paddingLeft: 2, paddingRight: 2, boxShadow: "1px 1px 1px #9E9E9E", flexWrap:'nowrap' }}>
            <div className='col-layout-justify-center'>
                <IconButton size="small" disabled={start <= 0 ? true : false} onClick={decrementStart}>
                    <ArrowLeft fontSize="inherit" />
                </IconButton>
            </div>
            {
                monthsArr.slice(start, start + viewItemCount).map((month, index) => <div key={index} className='col-layout-justify-center'><Button onClick={() => buttonClickAction(month)} variant={month === clicked ? "outlined" : ''} color={month === clicked ? "primary" : "default"} size="small">{month}</Button></div>)
            }
            <div className='col-layout-justify-center'>
                <IconButton size="small" disabled={start >= 9 ? true : false} onClick={incrementStart}>
                    <ArrowRight fontSize="inherit" />
                </IconButton>
            </div>
        </div>
    );
}

function PendingTransactionsPanel(props) {
    const [expanded, setExpanded] = React.useState(false);
    const PanelSummary = withStyles({
        root: {
            backgroundColor: 'rgba(0, 0, 0, .03)',
            borderBottom: '1px solid rgba(0, 0, 0, .125)',
            marginBottom: -1,
            minHeight: '10px',
            '&$expanded': {
                minHeight: '10px',
            },
        },
        content: {
            '&$expanded': {
                margin: '12px 0',
            },
        },
        expanded: {},
    })(ExpansionPanelSummary);

    const handleChange = panel => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
    };

    return (
        <div style={{ width: '100%' }}>
            <ExpansionPanel expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
                <PanelSummary
                    expandIcon={<ExpandMore htmlColor="white" />}
                    style={{ backgroundColor: '#3542b5', borderTopLeftRadius: 5, borderTopRightRadius: 5 }}
                >
                    <div className="row-layout" style={{ alignItems: 'center' }}>
                        <AccessAlarm htmlColor="white" />
                        <Link style={{ marginLeft: '10px', color: 'white' }}>View Pending Transactions</Link>
                    </div>
                </PanelSummary>
                <ExpansionPanelDetails>
                    <Label zeroMargin={true}>
                        No Pending Transactions.
                    </Label>
                </ExpansionPanelDetails>
            </ExpansionPanel>
        </div>
    );
}