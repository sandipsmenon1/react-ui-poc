import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import Avatar from '@material-ui/core/Avatar';
import { deepOrange } from '@material-ui/core/colors';
import LinearProgress from '@material-ui/core/LinearProgress';

const CustomerCard = (props ) => {
    const classes = useStyles();
    return (
        <Container className={classes.customerCard} maxWidth="lg">   
          <Grid container justify="center" alignItems="center">
              <Grid className={classes.tile} item lg={12} spacing={0}>
                <Grid item lg direction="column">
                    <Grid container justify="center" alignItems="center">
                        <Avatar className={classes.orangeAvatar}>JS</Avatar>
                    </Grid>
                    <Grid className={classes.content} item lg>
                        <span style={{fontSize: '25px'}}>Mr. Jon Snow</span>
                    </Grid>
                    <Grid className={classes.title} item lg>
                        <span>250, Abbey House, Royal Mint st, E12 5AS</span>
                    </Grid>
                    <Grid className={classes.title} item lg>
                        <span>+44 (0)7384212372</span>
                    </Grid>
                </Grid>
              </Grid>
          </Grid>
          <Grid container justify="center" alignItems="center">
            <Grid className={classes.tile} item lg={6} spacing={0}>
                <Grid item lg direction="column">
                    <Grid className={classes.title} item lg>
                        <span>Spend so for</span>
                    </Grid>
                    <Grid className={classes.amount} item lg>
                        <span>£420.12</span>
                    </Grid>
                </Grid> 
            </Grid>
            <Grid className={classes.tile} item lg={6} spacing={0}>
                <Grid item lg direction="column">
                    <Grid className={classes.title} item lg>
                        <span>Budgeted</span>
                    </Grid>
                    <Grid className={classes.amount} item lg>
                        <span>£2,500.00</span>
                    </Grid>
                </Grid> 
            </Grid>
          </Grid>
          <Grid container justify="center" alignItems="center">
            <Grid className={classes.tile} item lg={12} spacing={0}>
                <Grid item lg direction="column">
                    <Grid item lg>
                        <span>Keep spending. You can spend</span>
                    </Grid>
                    <Grid className={classes.amount} item lg>
                        <span>£2,079.88</span>
                    </Grid>
                    <Grid item lg>
                        <span>for the rest of the month</span>
                    </Grid>
                    <Grid item lg>
                        <LinearProgress className={classes.progress}  colorPrimary={deepOrange} variant="determinate" value={20} />
                    </Grid>
                </Grid> 
            </Grid>
          </Grid>
        </Container>
        );
};

const useStyles = makeStyles({
    customerCard: {
        padding: 0,
    },
    title: {
        color: '#909090',
        fontSize: "15px",
    },
    content: {
        fontSize: "22px",
    },
    tile: {
        border:'solid 1px #efedea',
        padding:25,
       
    },
    amount: {
        color: '#1b63b0',
        fontWeight: 'bold',
        fontSize: 22,
    },
    progress: {
        height: 20,
        marginTop:10,
        borderRadius:10,
    },
    orangeAvatar: {
      margin: 10,
      color: '#fff',
      fontSize:30,
      width: 100,
      height: 100,
      backgroundColor: deepOrange[500],
    },
  });   

CustomerCard.propTypes = {
    initialValues: PropTypes.string,
}

CustomerCard.defaultProps = {
    initialValues: '',
}

export default CustomerCard;

