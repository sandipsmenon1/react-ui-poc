import React, { PureComponent } from 'react';
import {
  LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
} from 'recharts';

const data = [
  {
    name: '01', Oct: 200,
  },
  {
    name: '02', Oct: 550, 
  },
  {
    name: '03', Oct: 800,
  },
  {
    name: '04', Oct: 0,
  },
  {
    name: '05', Oct: 400,
  },
  {
    name: '06', Oct: 700,
  },
  {
    name: '07', Oct: 1500,
  },
];

export default class Spendtrend extends PureComponent {

  render() {
    return (
      <LineChart width={250} height={150} data={data}>
        <CartesianGrid strokeDasharray="3 3" />
        <YAxis />
        <Tooltip />
        <Line type="monotone" dataKey="Oct" stroke="#1b63b0" strokeWidth={2} />
      </LineChart>
    );
  }
}
