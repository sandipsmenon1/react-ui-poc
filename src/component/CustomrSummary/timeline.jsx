import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles({
  root: {
    width: '100%',
    padding: 0,
  },
  stepper: {
    padding: 0,
  }
});

function getSteps() {
  const label =  [
      {
        Date: "1st, Nov",
        Payee: "Council Tax",
        Amount: "£130.00"
      },
      {
        Date: "5th, Nov",
        Payee: "Virgin Media",
        Amount: "£27.25"
      },
      {
        Date: "13th, Nov",
        Payee: "Credit Card",
        Amount: "£1431.20"
      },
      {
        Date: "18th, Nov",
        Payee: "Energy Supplier",
        Amount: "£54.00"
      }
      
  ]
  return label;
}


export default function HorizontalLabelPositionBelowStepper() {
  const classes = useStyles();
  const steps = getSteps();
  return (
    <div className={classes.root}>
      <Stepper className={classes.stepper} activeStep={5} alternativeLabel>
        {steps.map(key => (
          <Step key={key.Date}>
            <StepLabel>
             <span>{`${key.Payee} (${key.Date})`}</span>
             <span style={{fontSize: '18px', color: '#1b63b0'}}>{key.Amount}</span>
            </StepLabel>
          </Step>
        ))}
      </Stepper>
    </div>
  );
}