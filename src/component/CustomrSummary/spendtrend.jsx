import React, { PureComponent } from 'react';
import {
  LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
} from 'recharts';

const data = [
  {
    name: '01', Oct: 8000, amt: 0,
  },
  {
    name: '05', Oct: 6000, amt: 5,
  },
  {
    name: '10', Oct: 3000, amt: 10,
  },
  {
    name: '15', Oct: 2500, amt: 15,
  },
  {
    name: '20', Oct: 4000, amt: 20,
  },
  {
    name: '25', Oct: 1500, amt: 25,
  },
  {
    name: '30', Oct: 4125, amt: 30,
  },
];

export default class Spendtrend extends PureComponent {
  static jsfiddleUrl = 'https://jsfiddle.net/alidingling/exh283uh/';

  render() {
    return (
      <LineChart width={250} height={150} data={data}>
        <CartesianGrid strokeDasharray="3 3" />
        <YAxis />
        <Tooltip />
        <Line type="monotone" dataKey="Oct" stroke="#1b63b0" strokeWidth={2} />
      </LineChart>
    );
  }
}
