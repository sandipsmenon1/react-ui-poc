
import React from 'react'
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, Cell } from 'recharts'

const spendAnalysis = [{ category: 'In', amount: 8250.00 },
  { category: 'Out', amount: 6500.00 },
  { category: 'Pot', amount: 2000.00 }]

const barColors = [{ fill: '#5F277D', border: '#481167' },
  { fill: '#C13234', border: '#B02E30' },
  { fill: '#A7BF50', border: '#95AA46' },
  { fill: '#2B9C0A', border: '#2B9C0A' },
  { fill: '#0A159C', border: '#0A159C' },
  { fill: '#4F02F7', border: '#4F02F7' },
  { fill: '#1102F7', border: '#1102F7' }]

const IandETrend = () => {
  return (
    <BarChart width={250} height={200} data={spendAnalysis}
      margin={{ top: 20, right: 10, left: 10, bottom: 0 }} barSize={10}>
      <CartesianGrid strokeDasharray="3 3"/>
      <XAxis dataKey="category"/>
      <YAxis dataKey="amount"/>
      <Tooltip/>
      <Bar dataKey="amount">
        {
          spendAnalysis.map((entry, index) => (
            <Cell key={`cell-${index}`} fill={barColors[index].fill} stroke={barColors[index].border} />
          ))
        }
      </Bar>
    </BarChart>
  )
}

export default IandETrend;
