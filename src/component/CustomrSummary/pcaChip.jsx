import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Chip from '@material-ui/core/Chip';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import AccouncementIcon from '@material-ui/icons/Announcement';
import Timeline from './timeline';
import SpendTrend from './spendtrend';
import IandETrend from './IandETrend';
import Grid from '@material-ui/core/Grid';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useParams
  } from "react-router-dom";
  

const PCAChip = (props) => {
const classes = useStyles();
return (
<div>
    <div className={classes.root}>
      <ExpansionPanel defaultExpanded>
        <ExpansionPanelSummary
        className={classes.summary}
          expandIcon={<ExpandMoreIcon  color='primary'/>}
          aria-controls="panel1c-content"
          id="panel1c-header"
        >
          <div className={classes.productcolumn}>
            <Grid item lg justify="center" alignItems="center" direction="column">
              <Typography className={classes.productName}>NEO Premier Current Account</Typography>
              <Typography className={classes.heading}>101121 766562</Typography>
              <Typography className={classes.heading}> <AccouncementIcon /> Your have 11 total alerts</Typography>
            </Grid>
          </div>
          <div className={classes.graphcolumn}>
            <SpendTrend />
          </div>
          <div className={classes.amountcolumn}>
            <Typography className={classes.secondaryHeading}>£4215.20</Typography>
            <a  style={{fontSize:'12px'}} href="#accdetails" className={classes.link}>
                View statement
            </a>
          </div>
        </ExpansionPanelSummary>
        <Divider />
        <ExpansionPanelDetails className={classes.details}>
          <div className={classes.timelinecolumn}>
            <Grid className={classes.timelineHeader} item lg={12} spacing={0}>
                Planned Debits: Next 4 subscribed
            </Grid>
            <Timeline />
          </div>
          <div className={clsx(classes.column, classes.helper)}>
           <IandETrend />
          </div>
        </ExpansionPanelDetails>
        <Divider />
        <ExpansionPanelActions>
          <Button size="small" color="primary">
          <Link to="/spendanalytics">
              View trends
          </Link>
          </Button>
        </ExpansionPanelActions>
      </ExpansionPanel>
    </div>
</div>);

}

const useStyles = makeStyles(theme => ({
    root: {
      width: '100%',
      marginBottom: 20,
    },
    summary: {
        backgroundColor: '#f0f8ff',
    },
    icon: {
        backgroundColor: '#1b63b0',
    },
    heading: {
      fontSize: 15,
      color: '#1b63b0',
      marginBottom: 10,
    },
    productName: {
        fontSize: 18,
        color: '#000',
        marginBottom: 10,
    },
    timelineHeader: {
        marginBottom:15
    },
    secondaryHeading: {
      fontSize: 25,
      color: '#1b63b0',
    },
    icon: {
      verticalAlign: 'bottom',
      height: 20,
      width: 20,
    },
    details: {
      alignItems: 'center',
    },
    column: {
      flexBasis: '33.33%',
    },
    productcolumn: {
        flexBasis: '40%',
    },
    graphcolumn: {
        flexBasis: '40%',
    },
    amountcolumn: {
        flexBasis: '20%',
    },
    timelinecolumn: {
        flexBasis: '66.66%',
      },
    helper: {
      borderLeft: `2px solid ${theme.palette.divider}`,
      padding: theme.spacing(1, 2),
    },
    link: {
      color: theme.palette.primary.main,
      textDecoration: 'none',
      '&:hover': {
        textDecoration: 'underline',
      },
    },
  }));
  

PCAChip.propTypes = {
    initialValues: PropTypes.string,
}

PCAChip.defaultProps = {
    initialValues: '',
}

export default PCAChip;

