import React, { PureComponent } from 'react';
import {
  LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
} from 'recharts';

const data = [
  {
    name: '01', month: 100,
  },
  {
    name: '02', month: 2000, 
  },
  {
    name: '03', month: 2500,
  },
  {
    name: '04', month: 3000,
  },
  {
    name: '05', month: 4000,
  },
  {
    name: '06', month: 5500,
  },
  {
    name: '07', month: 6000,
  },
];

export default class Spendtrend extends PureComponent {

  render() {
    return (
      <LineChart width={250} height={150} data={data}>
        <CartesianGrid strokeDasharray="3 3" />
        <YAxis />
        <Tooltip />
        <Line type="monotone" dataKey="month" stroke="#1b63b0" strokeWidth={2} />
      </LineChart>
    );
  }
}
