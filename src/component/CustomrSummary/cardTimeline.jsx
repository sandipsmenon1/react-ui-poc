import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles({
  root: {
    width: '100%',
    padding: 0,
  },
  stepper: {
    padding: 0,
  }
});

function getSteps() {
  const label =  [
      {
        Type: "Due date",  
        Date: "4th, Nov",
      },
      {
        Type: "Statement date", 
        Date: "24th, Nov",
      },
  ]
  return label;
}


export default function HorizontalLabelPositionBelowStepper() {
  const classes = useStyles();
  const steps = getSteps();
  return (
    <div className={classes.root}>
      <Stepper className={classes.stepper} activeStep={5} alternativeLabel>
        {steps.map(key => (
          <Step key={key.Date}>
            <StepLabel>
             <span>{key.Date}</span>
            </StepLabel>
            <span style={{fontSize: '18px', color: '#1b63b0'}}>{key.Type}</span>
          </Step>
        ))}
      </Stepper>
    </div>
  );
}