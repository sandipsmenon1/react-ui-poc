import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles({
  root: {
    width: '100%',
    padding: 0,
  },
  stepper: {
    padding: 0,
  }
});

function getSteps() {
  const label =  [
      {
        Date: "2st, Jul",
        Amount: "£1500"
      },
      {
        Date: "4th, Aug",
        Amount: "£1000"
      },
      {
        Date: "3th, Sep",
        Amount: "£1200"
      },
      {
        Date: "5th, Oct",
        Amount: "£900"
      },
  ]
  return label;
}


export default function HorizontalLabelPositionBelowStepper() {
  const classes = useStyles();
  const steps = getSteps();
  return (
    <div className={classes.root}>
      <Stepper className={classes.stepper} activeStep={5} alternativeLabel>
        {steps.map(key => (
          <Step key={key.Date}>
            <StepLabel>
             <span>{key.Date}</span>
            </StepLabel>
            <span style={{fontSize: '18px', color: '#1b63b0'}}>{key.Amount}</span>
          </Step>
        ))}
      </Stepper>
    </div>
  );
}