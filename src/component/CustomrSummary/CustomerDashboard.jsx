import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Header from '../header/header';
import CustomerCard from './customerCard';
import PCAChip from './pcaChip';
import SavingsChip from './savingsChip';
import CreditChip from './CreditCardChip';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';

const CustomerDashboard = (props) =>  {
const classes = useStyles();
return (
    <div>
        <Header />
        <div className="container">
            <Grid className={classes.DashboardContainer} container spacing={2}>
                <Grid item lg={4} >
                    <CustomerCard />
                </Grid>
                <Grid item direction="column" lg={8}>
                   <PCAChip />
                   <SavingsChip />
                   <CreditChip />
                </Grid>
            </Grid>
        </div>
    </div>
    
);

}

const useStyles = makeStyles({
    DashboardContainer: {
        marginTop: 20,
    },
});   


CustomerDashboard.propTypes = {
    initialValues: PropTypes.string,
}

CustomerDashboard.defaultProps = {
    initialValues: '',
}

export default CustomerDashboard;

