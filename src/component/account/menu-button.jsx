import React, { useState } from 'react'
import Button from '@material-ui/core/Button'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'

const MenuButton = ({ name, accounts }) => {
  const [menu, setMenu] = useState(undefined)

  const handleClick = (event) => {
    setMenu(event.currentTarget)
  }

  const handleClose = () => {
    setMenu(undefined)
  }

  return (
    <React.Fragment>
      <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick} variant="contained"
        color="primary">
        Change
      </Button>
      <Menu
        id="simple-menu"
        anchorEl={menu}
        keepMounted
        open={Boolean(menu)}
        onClose={handleClose}
      >
        { accounts && accounts.map(account => <MenuItem onClick={handleClose}>{`${account.accountName} - ${account.accountNumber}`}</MenuItem>)}
      </Menu>
    </React.Fragment>)
}

export default MenuButton
