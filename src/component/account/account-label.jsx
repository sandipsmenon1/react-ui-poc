import React from 'react'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import MenuButton from './menu-button'
import analysticsData from '../../data-objects/spendAnalytics.json'
import DateFnsUtils from '@date-io/date-fns'
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from '@material-ui/pickers'
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';

const useStyles = makeStyles({
  accountLabel: {
    textAlign: 'left',
    color: '#006a4d'
  },
  accountContainer: {
    marginBottom: '20px'
  },
  alignLeft: {
    textAlign: 'left'
  },
  marginLeft: {
    marginLeft: '90px'
  },
  link: {
    textDecoration: 'none',
    '&:hover': {
      textDecoration: 'underline',
    },
  }
})

const AccountLabel = ({ name }) => {
  const [fromDate, setFromDate] = React.useState(new Date())
  const [toDate, setToDate] = React.useState(new Date())

  const handleFromDateChange = date => {
    setFromDate(date)
  }
  const handleToDateChange = date => {
    setToDate(date)
  }
  const classes = useStyles()
  return (
    <React.Fragment>
      <Typography component={'p'} color="textSecondary" className={classes.alignLeft}>
        {'Your Account'}
      </Typography>
      <Grid container spacing={4} className={classes.accountContainer}>
        <Grid item xs={16} md={6} lg={4}>
          <Typography component={'h4'} variant={'h5'} color='primary' className={classes.accountLabel}>
            {name}
          </Typography>
          <Typography component={'p'} variant={'caption'} color='primary' className={classes.accountLabel}>
             <a  style={{fontSize:'15px'}} href="#dashboard" className={classes.link}>
               <ArrowBackIosIcon/>
                Back to summary
             </a>
          </Typography>
        </Grid>
        <Grid item xs={6} md={2} lg={1} className={classes.alignLeft}>
          <MenuButton accounts={analysticsData.accounts}/>
        </Grid>
        <Grid item xs={32} md={10} lg={7} className={classes.alignLeft}>
          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <Grid container justify='flex-end'>
              <KeyboardDatePicker
                disableToolbar
                variant="inline"
                format="MM/dd/yyyy"
                margin="normal"
                id="date-picker-inline"
                label="Date From"
                value={fromDate}
                onChange={handleFromDateChange}
                KeyboardButtonProps={{
                  'aria-label': 'change date'
                }}
              />

              <KeyboardDatePicker
                disableToolbar
                variant="inline"
                format="MM/dd/yyyy"
                margin="normal"
                id="date-picker-inline"
                label="Date To"
                value={toDate}
                onChange={handleToDateChange}
                KeyboardButtonProps={{
                  'aria-label': 'change date'
                }}
                className={classes.marginLeft}
              />
            </Grid>
          </MuiPickersUtilsProvider>
        </Grid>
      </Grid>
    </React.Fragment>)
}

export default AccountLabel
