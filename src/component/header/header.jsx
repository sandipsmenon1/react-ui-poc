import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useParams
} from "react-router-dom";
import Badge from '@material-ui/core/Badge';
import MailIcon from '@material-ui/icons/Mail';
import PersonIcon from '@material-ui/icons/Person';
import NotificationsIcon from '@material-ui/icons/Notifications';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    margin: {
     marginRight: '20px'
    },
    padding: {
      padding: theme.spacing(0, 2),
    },
  }));

const Header = (props) => {
const classes = useStyles();
return(
    <div>
        <div class="top-bar py-2 border-bottom">
            <div class="container">
                <div class="row middle-flex">
                    <div class="col" style={{ textAlign: 'right', padding: '0px 5px' }}>
                        <div style={{ float: 'right' }}>
                            <Link to="/"><a href="" class="btn login-button-2 text-uppercase text-wh">
                                <span class="fa fa-sign-in mr-2"></span>Logout</a></Link>
                        </div>
                        <p style={{ color: 'white', margin: '5px' }}>
                            <Badge className={classes.margin} badgeContent={4} badgeContent={4} color="primary">
                                <NotificationsIcon />
                            </Badge>
                            <Badge className={classes.margin} variant="dot" color="primary">
                                <MailIcon />
                            </Badge>
                            <PersonIcon />
                           <span style={{margin: '0 10px' }}>Welcome!! Mr. JON SNOW</span> 
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-top py-1">
            <div class="container">
                <div class="nav-content">
                    <h1>
                        <Link to=""><img src={require('../../Assets/Neobank_final.png')} style={{ width: '70%' }} /></Link>
                    </h1>
                    <div class="nav_web-dealingsls">
                        <nav>
                            <label for="drop" class="toggle">Menu</label>
                            <input type="checkbox" id="drop" />
                            <ul class="menu">
                                <li>
                                    <label for="drop-1" class="toggle toogle-2">Products and services <span class="fa fa-angle-down"
                                        aria-hidden="true"></span>
                                    </label>
                                    <a href="#">Products and services</a>
                                    <input type="checkbox" id="drop-1" />
                                    <ul>
                                        <li><a class="drop-text" href="#">Everyday Banking</a></li>
                                        <li><a class="drop-text" href="#">Ways to bank</a></li>
                                        <li><a class="drop-text" href="#">Borrowing</a></li>
                                        <li><a class="drop-text" href="#">Offers & rewards</a></li>
                                        <li><a class="drop-text" href="#">Insurance</a></li>
                                        <li><a class="drop-text" href="#">International services</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <label for="drop-2" class="toggle toogle-2">Help and Support <span class="fa fa-angle-down"
                                        aria-hidden="true"></span>
                                    </label>
                                    <a href="#">My Banking</a>
                                    <input type="checkbox" id="drop-2" />
                                    <ul>
                                        <li><a class="drop-text" href="#">Move Money</a></li>
                                        <li><a class="drop-text" href="#">Mortgage applications</a></li>
                                        <li><a class="drop-text" href="#">Card services</a></li>
                                        <li><a class="drop-text" href="#">Account services</a></li>
                                        <li><a class="drop-text" href="#">Documents</a></li>
                                        <li><a class="drop-text" href="#">My profile</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <label for="drop-3" class="toggle toogle-2">Banking with us <span class="fa fa-angle-down"
                                        aria-hidden="true"></span>
                                    </label>
                                    <a href="#pages">Investments & Life Events<span class="fa fa-angle-down" aria-hidden="true"></span></a>
                                    <input type="checkbox" id="drop-3" />
                                    <ul>
                                        <li><a class="drop-text" href="#">My Investments</a></li>
                                        <li><a class="drop-text" href="#">Investing</a></li>
                                        <li><a class="drop-text" href="#">Life events</a></li>
                                        <li><a class="drop-text" href="#">Support</a></li>
                                    </ul>
                                </li>
                                <li><a href="">Contact NEO</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
);
};
    
    


export default Header;
