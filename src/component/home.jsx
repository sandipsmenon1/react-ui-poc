import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Header from './LandingPage/header';
import Footer from './LandingPage/footer';
import Content from './LandingPage/content';
import Carousel from './LandingPage/Carousel';
import './LandingPage/index.css';
import './LandingPage/bootstrap.css'
import './LandingPage/sliders.css'
import banner_img1 from './LandingPage/img1.jpg'
import banner_img2 from './LandingPage/img2.jpg'
import banner_img3 from './LandingPage/img3.jpg'
const banners = [{"id":1, "text":"Welcome to Neo bank","text2":"All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary", "image_url":banner_img1,"redirect_url":"","sequence_number":1,"status":"active","external_url":true,"name":""},{"id":2, "text":"Investing","text2":"It is a long established fact that a reader will be distracted by the readable content of a page", "image_url":banner_img2,"redirect_url":"","sequence_number":2,"status":"active","external_url":false,"name":""},{"id":3,"text":"Customer Support","text2":"The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested","image_url":banner_img3,"redirect_url":"","sequence_number":3,"status":"active","external_url":true,"name":""}]



class Home extends Component {

constructor(props) {
    super(props);
    this.state = {};
}

render() {
    console.log(this.props.initialValues);
    return (<div>
      <Header/>
      <Carousel banners={banners}/>
    <Content/>
    <Footer/>
    </div>);
}

}

Home.propTypes = {
    initialValues: PropTypes.string,
}

Home.defaultProps = {
    initialValues: '',
}

export default Home;
