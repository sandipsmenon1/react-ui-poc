import { connect } from 'react-redux';
import Header from '../component/header/header';


const mapStateToProps = state => ({
    initialValues: state.app.login,
});

export default connect(mapStateToProps)(Header);

