import { connect } from 'react-redux';
import SpendAnalytics from '../component/SpendAnalytics';


const mapStateToProps = state => ({
    initialValues: state.app.login,
});

export default connect(mapStateToProps)(SpendAnalytics);

