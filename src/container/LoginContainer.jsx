import { connect } from 'react-redux';
import Login from '../component/Login';


const mapStateToProps = state => ({
    initialValues: state.app.login,
});

export default connect(mapStateToProps)(Login);

