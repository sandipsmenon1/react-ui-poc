import { connect } from 'react-redux';
import AccountDetails from '../component/AccountDetails';


const mapStateToProps = state => ({
    initialValues: state.app.login,
});

export default connect(mapStateToProps)(AccountDetails);

