import { CLEAR_STATE } from '../constants/action-types'

export const clearState = () => ({
  type: CLEAR_STATE
})
