import { CLEAR_STATE } from '../constants/action-types';

const initialState = {
    login: 'success'
}

export default function app(state = initialState, action = {}) {

    switch (action.type) {
        case CLEAR_STATE:
            return initialState;
        default:
            return initialState
    }
}
