import React, {Component} from 'react';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import Home from '../container/HomeContainer'
import Login from '../container/LoginContainer'
import CustomerDashboard from '../container/CustDashboardContainer';
import AccountDetails from '../container/AccountDetailsContainer';
import SpendAnalytics from '../container/SpendAnalyticsContainer'

class AppRoute extends Component {
    render() {
        console.log("In app-routes");
        return(
            <div className="app-wrapper">
               <Router>
                <Switch>
                    <Route 
                        exact
                        path='/'
                        render={props => <Home {...props} />}
                    />
                    <Route 
                        exact
                        path='/login'
                        render={props => <Login {...props} />}
                    />
                    <Route
                        exact
                        path='/dashboard'
                        render={props => <CustomerDashboard {...props} />}
                    />
                    <Route
                        exact
                        path='/accdetails'
                        render={props => <AccountDetails {...props} />}
                    />
                    <Route 
                        exact
                        path='/spendanalytics'
                        render={props => <SpendAnalytics {...props} />}
                    />
                </Switch>
               </Router>
            </div>
        );
    }
}
export default AppRoute;