'use strict'
const chai = require('chai');
var assert = chai.assert;

class loginPages {

    open(path) {
        browser.url(path);
        browser.pause(1000);
    }

    click_login() {
        var login_link = $('//a[@class="btn login-button-2 text-uppercase text-wh"]')
        login_link.click();
        browser.pause(3000);
    }

    login(usr_nm, pwd) {
        //@@@ verify how to define at a common place
        var user_name = $('//input[@name="name"]')
        var password = $('//input[@name="password"]')
        var login_btn = $('//button[@class="btn submit mb-4"]')

        user_name.setValue(usr_nm);
        password.setValue(pwd)
        browser.pause(3000);
        login_btn.click()
        browser.pause(3000);

    }

    //@@@ use this as a common function
    isExiting(ele) {
        console.log('Pass');
    }

    verifyNavigation() {

        var statements = $('*=statement')
        statements.click()
        browser.pause(3000);

        var back_to_sum = $('/html/body/div/div/div/div/div[2]/div[1]/a')
        back_to_sum.scrollIntoView();
        browser.pause(3000);
        back_to_sum.click()
        browser.pause(3000);

        var view_trends = $('/html/body/div/div/div/div/div[2]/div/div[2]/div[1]/div/div/div[2]/div/div/div/div[2]/button/span[1]/a')
        view_trends.scrollIntoView();
        browser.pause(3000);
        view_trends.click()
        browser.pause(3000);

        var logout = $('/html/body/div[1]/div/div/div/div/div[1]/div/div/div/div/a/a')
        logout.scrollIntoView();

        var back1 = $('/html/body/div[1]/div/div/div/main/div/div[1]/div[1]/p/a')
        back1.scrollIntoView();
        browser.pause(3000);
        back1.click()
        browser.pause(3000);
    }

    verifyNavigation1() {
        var pot_summary = $('/html/body/div/div/div/div/div[2]/div/div[2]/div[2]/div/div/div[1]/div[1]/div[3]/a')
        pot_summary.scrollIntoView();
        browser.pause(3000);
        pot_summary.click()
        browser.pause(3000);

        var back2 = $('/html/body/div/div/div/div/div[2]/div[1]/a')
        back2.scrollIntoView();
        browser.pause(3000);
        back2.click();
        browser.pause(3000);
    }
    verifyNavigation2() {
        var card_stmt = $('/html/body/div/div/div/div/div[2]/div/div[2]/div[3]/div/div/div[1]/div[1]/div[3]/a')
        card_stmt.scrollIntoView();
        browser.pause(3000);
        card_stmt.click();
        browser.pause(3000);

        var back3 = $('/html/body/div/div/div/div/div[2]/div[1]/a')
        back3.scrollIntoView();
        browser.pause(3000);
        back3.click();
        browser.pause(3000);
    }

    mouseHover(linkName) {

        switch (linkName) {
            case 'Product_and_Services':
                var Product_and_Services = $('=Products and services')
                Product_and_Services.moveTo()
                browser.pause(3000);
                break;

            case 'Help_and_Support':
                var Help_and_Support = $('=Help and Support')
                Help_and_Support.moveTo()
                browser.pause(3000);
                break;

            case 'Banking_with_us':
                var Banking_with_us = $('*=Banking with')
                Banking_with_us.moveTo()
                browser.pause(3000);
                break;

        }

    }

    verifySubMenus(listofItems, linkName) {
        switch (linkName) {
            case 'Product_and_Services':
                var text = $$('//ul[@class="menu"]/li[1]/ul/li');
                listofItems.forEach(function (element, i) {
                    assert.equal(listofItems.includes(text[i].$('a').getText().trim()), true);
                });
                break;
        }

    }

    verifyL1Menus(var1) {

        switch (var1) {
            case 'Personal':
                var tmp1 = $('//li[@class="current"]')
                assert.equal(tmp1.getText(), 'Personal');
                break;
            case 'Business':
                var tmp1 = $('=Business')
                assert.equal(tmp1.isExisting(), true);
                break;
            case 'Private Banking':
                var tmp1 = $('=Private Banking')
                assert.equal(tmp1.isExisting(), true);
                break;
            case 'International Banking':
                var tmp1 = $('=International Banking')
                assert.equal(tmp1.isExisting(), true);
                break;
        }
    }

}

module.exports = loginPages;