var loginPage = require('./loginPage.js');
const { Given, When, Then } = require('cucumber');
var login_Pg = new loginPage();

Given(/^I enter the "([^"]*)"$/, function (url) {
    login_Pg.open(url);
    browser.maximizeWindow()
});

When(/^I tap on the login button and enter "([^"]*)" and "([^"]*)"$/, function (usr_id, pwd) {
    login_Pg.click_login();
    login_Pg.login(usr_id, pwd);
});

Then(/^I should see the login page loaded$/, function () {
    login_Pg.isExiting('logout')
});

When(/^I hover on the "([^"]*)" link$/, function (param) {
    login_Pg.mouseHover(param)
});

Then(/^I should see "([^"]*)" of L2 menus for "([^"]*)"$/, function (listofValues, param)  {
    login_Pg.verifySubMenus(listofValues.split(','),param)
});

When(/^I should land on the defalut page$/, function ()  {
    console.log('Success')
});

When(/^I verify navigation to statements$/, function ()  {
    login_Pg.verifyNavigation()
});

When(/^I verify navigation to pot summary$/, function ()  {
    login_Pg.verifyNavigation1()
});

When(/^I verify navigation to card statement$/, function ()  {
    login_Pg.verifyNavigation2()
});

Then(/^I should see the link "([^"]*)"$/, function (var1)  {
     login_Pg.verifyL1Menus(var1);
});


