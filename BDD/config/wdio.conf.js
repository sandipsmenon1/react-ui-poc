exports.config = {
    runner: 'local',
    path: '/',
    specs: [
        './feature/*.feature'
    ],
    exclude: [
    ],
    maxInstances: 2,
    capabilities: [{
        maxInstances: 2,
        browserName: 'chrome',
    }],
    logLevel: 'info',
    bail: 0,
    baseUrl: '',
    waitforTimeout: 10000,
    connectionRetryTimeout: 90000,
    connectionRetryCount: 3,
    services: ['chromedriver'],
    framework: 'cucumber',
    cucumberOpts: {
        requireModule: [],
        require: ['./stepDefinition/stepDefinition.js'],
        backtrace: true,
        compiler: [],
        failAmbiguousDefinitions: true,
        dryRun: false,      // <boolean> invoke formatters without executing steps
        failFast: false,    // <boolean> abort the run on first failure
        ignoreUndefinedDefinitions: false,    // <boolean> Enable this config to treat undefined definitions as warnings
        name: [],           // <string[]> ("extension:module") require files with the given EXTENSION after requiring MODULE (repeatable)
        snippets: true,     // <boolean> hide step definition snippets for pending steps
        format: ['pretty'], // <string[]> (type[:path]) specify the output format, optionally supply PATH to redirect formatter output (repeatable)
        colors: false,       // <boolean> disable colors in formatter output
        snippets: false,    // <boolean> hide step definition snippets for pending steps
        source: false,      // <boolean> hide source uris
        profile: [],        // <string[]> (name) specify the profile to use
        strict: true,       // <boolean> fail if there are any undefined or pending steps
        tagExpression: 'not @Pending',      // <string> (expression) only execute the features or scenarios with tags matching the expression, see https://docs.cucumber.io/tag-expressions/
        timeout: 30000,    // <number> timeout for step definitions
        tagsInTitle: false,                 // <boolean> add cucumber tags to feature or scenario name
        snippetSyntax: undefined,           // <string> specify a custom snippet syntax
    },
    reporters: [

        ['cucumberjs-json', {
            jsonFolder: './reports/',
            language: 'en',
        },
        ],
    ],
    // mochaOpts: {
    //     ui: 'bdd',
    //     timeout: 60000
    // },

}
