Feature: Verify L1 & L2 tabs for Personal Accounts

    Scenario Outline: Validate the sub menus in Personal Accounts page
        Given I enter the "<url>"
        When I should land on the defalut page
        Then I should see the link "Personal"
        Then I should see the link "Business"
        Then I should see the link "Private Banking"
        Then I should see the link "International Banking"
        When I hover on the "<linkName>" link
        Then I should see "<list>" of L2 menus for "<linkName>"

        Examples:
            | url                                              | user_id  | password   | linkName             | list                                                                         |
            | ec2-13-59-65-238.us-east-2.compute.amazonaws.com | Aravinth | CorrectPwd | Product_and_Services | Current accounts,Credit cards,Saving accounts,Investing,Retirement,Loans     |
            | ec2-13-59-65-238.us-east-2.compute.amazonaws.com | Aravinth | CorrectPwd | Help_and_Support     | By your side,Customer support,Service status,Ask the experts,Product support |
            | ec2-13-59-65-238.us-east-2.compute.amazonaws.com | Aravinth | CorrectPwd | Banking_with_us      | ways to bank,Who we are,In your community,Mobile branches                    |

