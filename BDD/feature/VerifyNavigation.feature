Feature: Verify Navigation for different screens

    Scenario Outline: Login - Landing Page
        Given I enter the "<url>"
        When I tap on the login button and enter "<user_id>" and "<password>"
        Then I should see the login page loaded
        Then I verify navigation to statements
        Then I verify navigation to pot summary
        Then I verify navigation to card statement


        Examples:
            | url                                              | user_id  | password   |
            | ec2-13-59-65-238.us-east-2.compute.amazonaws.com | Aravinth | CorrectPwd |           
